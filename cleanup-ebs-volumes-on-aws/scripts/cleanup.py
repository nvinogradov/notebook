import boto3

total = 0
ec2 = boto3.client('ec2', region_name='us-west-2')
response = ec2.describe_volumes(MaxResults=100)
count = 0
while response:
    for volume in response['Volumes']:
        if volume['State'] == 'available':
            try:
                ec2.delete_volume(
                    VolumeId=volume['VolumeId']
                )
                count += 1
            except Exception as e:
                print(e)
    response = ec2.describe_volumes(MaxResults=100, NextToken=response['NextToken']) if 'NextToken' in response else None
if count > 0:
    print(f"{count} volumes found in customer1-dev")
    total += count
print(f"removed {total} volumes")