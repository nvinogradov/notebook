# Clean up EBS volumes on AWS
Follow these steps to, with the help of Boto3 SDK running within a docker container, delete all "Available" (as opposed to "In Use") EBS volumes.

First, Boto3 will need your credentials to access the target AWS environment: 
* Find your environment on [AWS start page](https://idsgrp.awsapps.com/start#/) then click "Command line or programmatic access"
   
   ![image](aws-start-page.png "Click \"Command line or programmatic access\"")
* You will see your authentication credentials. Open your terminal of choice and export them as environment variables.
* In addition, export `AWS_DEFAULT_REGION=us-west-2`, setting the appropriate region value.

That's it, we are ready to run the script. To do, simply execute the command below from the directory containing this guide:
```
docker run -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e AWS_DEFAULT_REGION -e AWS_SESSION_TOKEN -v $PWD/scripts:/tmp ombu/boto3:1.8 python cleanup.py
``` 
It may take a while (up to 5 minutes for every 1000 volumes) but if you are not seeing any errors that means the script is working.